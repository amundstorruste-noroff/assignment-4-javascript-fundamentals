# Assignment 4

Author: Amund Kulsrud Storruste

The project contains the delivery for Assignment 4 in the Experis Academy accellerate course at Noroff University.

## Installation

The project uses HTML, CSS, and JS that runs in browser.

Open the index.html file in Google Chrome (or another browser) to start the application.

## Usage

The application simulates a banking, working ecosystem in a dystopian Capitalist future. 
The only pursuit of mankind has been reduced to clicking buttons and purchasing outdated computers.

Click the `Work` button to generate 100 NOK of pay.

Click the `Bank` button to move all generated pay into a McDuck Holdings Inc. Bank account.

Click the `Get a loan` button to further drive mankind into dept.

Click the `Repay loan` to pay back the dirty money you owe Scrooge McDuck before he sends his mafioso at you. 

Click the `Buy now` button to spend your hard earned money on some sweet hardware.


## Wireframe

The following wireframe was supplied in the assignment description. The application was built based on it.

<img src="sample_wireframe.PNG"/>