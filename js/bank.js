const showLoanBalance = () => {
    let element = document.getElementById("loanDisplay");
    element.style.visibility = "visible";
}

const hideLoanBalance = () => {
    let element = document.getElementById("loanDisplay");
    element.style.visibility = "hidden";
}


/** Event handler for getting a loan */ 
const HandleGetLoanButton = () => {
    document.getElementById("loan-button").addEventListener("click", (event) => {
    // Get current Balance 
    const balanceElement = document.getElementById("balance");
    let balanceValue = parseInt(balanceElement.innerHTML.split(' ').shift());

    // Get current Loan
    const loanElement = document.getElementById("loan");
    let loanValue = parseInt(loanElement.innerHTML.split(' ').shift());
    
    // Check wanted loan value
    let newLoan = parseInt(window.prompt("How much do you want to lend?"));
    if (isNaN(newLoan)) return; 
    
    // Constraints
    // #1 Loan cannot be double of bank balance
    if (newLoan > 2*balanceValue) return;
    // #2 You cannot get more than one bank loan
    // #3 You may not have two loans at once
    if (loanValue > 0) return;

    // Update bank account
    loanElement.innerHTML = loanValue + newLoan;
    loanElement.innerHTML += " NOK";
    
    balanceElement.innerHTML = balanceValue + newLoan;
    balanceElement.innerHTML += " NOK"


    // Show loan value if it is positive
    if (loanValue + newLoan > 0) {
        showRepayLoanButton();
        showLoanBalance();
    }
    });
}
