/* Start by hiding elements that shouldn't be shown*/
hideRepayLoanButton();
hideLoanBalance();

/* Populate laptop dropdown*/
populateDropdown();

/* Activate button handlers*/
HandleGetLoanButton();
HandleRepayLoanButton();
HandleTransferButton()
HandleWorkButton();
HandleBuyButton();
HandleDropdownSelection();