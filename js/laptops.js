// Make Laptops selection
const apiUrl = "https://noroff-komputer-store-api.herokuapp.com/computers";
const imgUrl ="https://noroff-komputer-store-api.herokuapp.com/"

// Fetch response (computers) from url and convert to json-array 
const fetchErrthingAsync = async (url) => {
    try {
        let response = await fetch(url);
        jsonArray = await response.json()
        return jsonArray
    }
    catch (e) {
        console.log(e.message);
    }
}

const populateDropdown = async () => {
    // Fetch all computers
    const computers = await fetchErrthingAsync(apiUrl);
    const computerNames = [];

    // Get dropdown
    const dropdown = document.getElementById("dropdown");

    dropdown.innerHTML = "";
    // Extract computer names
    for (const computer of computers)
    {
        const option = `<option>${computer.title}</option>`;
        dropdown.innerHTML += option;
    }

    // After loading display product information. First element by default
    displayProductInformationInBigBoi(computers[0]);
    displayFeatureInformationInSmolBoi(computers[0]);
}

// Create event handler for Laptop selection
const HandleDropdownSelection = () => {
    document.getElementById("dropdown").addEventListener("change", async (event) => {
        // Get the selected name from the list of computer names
        const computerName = event.target.value;
        
        // Now fetch all computers 
        const computers = await fetchErrthingAsync(apiUrl);

        // Match with selected computer
        const selectedComputer = computers.filter( (computer) => 
            computer.title === computerName
        )

        // Display this computer
        displayProductInformationInBigBoi(selectedComputer[0]);
        displayFeatureInformationInSmolBoi(selectedComputer[0]);
    });
}

const displayProductInformationInBigBoi = (computer) => {
    const imgUrl ="https://noroff-komputer-store-api.herokuapp.com/" + computer.image;

    // Update image
    const laptopImage = document.getElementById("laptop-image");
    laptopImage.src = imgUrl;

    // Update description
    var title = document.querySelector("#laptop-description p:nth-child(1)");
    title.innerHTML = computer.title;

    var description = document.querySelector("#laptop-description p:nth-child(3)");
    description.innerHTML = computer.description;

    // Update price
    const price = document.getElementById("laptop-price");
    price.innerHTML = computer.price + " " + "NOK";
}

const displayFeatureInformationInSmolBoi = ({specs}) => {
    const features = document.getElementById("feature-list");     
    features.innerHTML = "";
    for (const spec of specs) {
        const li = `<p>${spec}</p>`;  
        features.innerHTML += li;
    }
}

// Make work
const HandleBuyButton = () => {
    document.getElementById("buy-button").addEventListener("click", (event) => {
        // Check cost
        const priceElement = document.getElementById("laptop-price");
        let price = parseInt(priceElement.innerHTML.split(' ').shift());

        // Check balance
        const balanceElement = document.getElementById("balance");
        let balanceValue = parseInt(balanceElement.innerHTML.split(' ').shift());

        // If broke --> Display message
        if ( price > balanceValue) {
            alert("You cannot afford the Laptop! Choose a cheaper version or get a job through Experis Academy!");
            return
        }
        // If sufficient money
        balanceValue = balanceValue - price;
        balanceElement.innerHTML = balanceValue + " NOK";
    
        laptopName = document.getElementById("laptop-title").innerHTML;
        alert("What a great purchase! You are now the proud owner of " + laptopName + "!");
    });
}


