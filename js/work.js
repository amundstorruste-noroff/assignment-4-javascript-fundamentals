// Make work
const HandleWorkButton = () => {
    document.getElementById("work-button").addEventListener("click", (event) => {
        // Get current Loan balance 
        const payElement = document.getElementById("pay");
        let payValue = parseInt(payElement.innerHTML.split(' ').shift());
        payElement.innerHTML = payValue + 100;
        payElement.innerHTML += " NOK";
    });
}

// Transfer money to bank account
const HandleTransferButton = () => {
    document.getElementById("transfer-button").addEventListener("click", (event) => {
        // Get bank balance
        const balanceElement = document.getElementById("balance");
        let balanceValue = parseInt(balanceElement.innerHTML.split(' ').shift());
        
        // Get work balance
        const payElement = document.getElementById("pay");
        let payValue = parseInt(payElement.innerHTML.split(' ').shift());

        // Get current Loan
        const loanElement = document.getElementById("loan");
        let loanValue = parseInt(loanElement.innerHTML.split(' ').shift());

        // Constraints
        // #1 If outstanding loan. Must transfer 10% to pay off loan.
        if (loanValue > 0) {
            // Pay loan
            let loanPayment = payValue / 10;
            loanValue = loanValue - loanPayment;
            loanElement.innerHTML = loanValue + " NOK";

            // Hide repay loan if loan has been repayed
            if (loanValue <= 0) hideRepayLoanButton();

            // Subtract from payment
            payValue = payValue - loanPayment;
        }

        // Update monies
        balanceValue += payValue;
        payValue = 0;

        // Update elements
        balanceElement.innerHTML = balanceValue + " NOK";
        payElement.innerHTML = payValue + " NOK";
    });
}

const hideRepayLoanButton = () => {
    const loanButton = document.getElementById("repay-button");
    loanButton.style.visibility = "hidden";
}

const showRepayLoanButton = () => {
    const loanButton = document.getElementById("repay-button");
    loanButton.style.visibility = "visible";
}

// Gotta still repay loan bro
const HandleRepayLoanButton = () => {
    document.getElementById("repay-button").addEventListener("click", (event) => {
        // Get work balance
        const payElement = document.getElementById("pay");
        let payValue = parseInt(payElement.innerHTML.split(' ').shift());

        // Get current Loan
        const loanElement = document.getElementById("loan");
        let loanValue = parseInt(loanElement.innerHTML.split(' ').shift());
        
        // Calculate new loan
        let newLoanValue = loanValue - payValue; 

        // Update elements
        loanElement.innerHTML = newLoanValue + " NOK";
        payElement.innerHTML = 0 + " NOK";

        // Hide repay loan button if loan has been repayed
        if (newLoanValue <= 0) {
            hideRepayLoanButton();
            hideLoanBalance();
        }
    });
}
